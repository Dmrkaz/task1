create table student(
id int primary key,
name varchar(255),
phone varchar(255),
group int);

create table groups(
id int primary key,
name varchar(255)
);

insert into groups (id,name)
values(1,'2001'),
(2,'2002'),
(3,'2003')
(4,'2004'),
(5,'2005'),
(6,'2006'),
(7,'2007'),
(8,'2008'),
(9,'2009'),
(10,'2010');
insert into student(id,name,phone,group)
values(1,'Bota','87057773342',1),
(2,'Zheka','87057773341',2),
(3,'Aseka','87057773343',3),
(4,'Aida','87057773344',4),
(5,'Aidana','87057773345',5),
(6,'Damir','87057773346',6),
(7,'Merey','87057773347',7),
(8,'Zhandos','87057773348',8),
(9,'Jhon','87057773349',9),
(10,'Alim','87057773350',0);
public class Student {
    private int id;
    private String name;
    private String phone;
    private int group;

    public Student(int id, String name, String phone, int group){
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.group = group;
    }

    @Override
    public String toString() {
        return "Student: " +
                "name = " + name +
                " phone = " + phone;
    }

    public Student(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }


}
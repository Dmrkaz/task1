import java.sql.*;

public class Main {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://127.0.0.1:5432/postgres";
        String user = "postgres";
        String pass = "123";
        Connection connection = null;

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, pass);
            connection.setAutoCommit(false);
            System.out.println("connected");
            Statement statement = null;
            statement=connection.createStatement();
            ResultSet resultSet=statement.executeQuery("SELECT * FROM STUDENTS ");
            while (resultSet.next()){
                System.out.println(resultSet.getString("name"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
